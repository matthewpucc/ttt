use strict;
use warnings;

use v5.20;

use Moops;
use Data::GUID;

use TTT::Game;

class TTT::API extends Mojolicous::Controller {
  has missing_type_message => (
    isa => Str,
    is => 'ro',
    default => "Please specify an output format",
  );

  method return_game (TTT::Game $game) {
    $self->respond_to(
      json => $game->as_json,
      xml => $game->as_xml,
      any => $self->missing_type_message,
    );
  }

  # basic rest interface
  method get () {
    my $game_id = $self->param( 'game_id' ) // $self->session( 'game_id' );

    if ($game_id) {
      $self->return_game($self->get_game($game_id));
    }
  }

  method put () {
    my $game = $self->new_game($self->param( 'player_id' ));

    $self->session( game_id => $game->id );
    $self->return_game( $game );
  }

  method post () { 
    my $game_id = $self->param( 'game_id' ) // $self->session( 'game_id' );

    if ($game_id) {

    $self->return_game( 
      $self->make_move( $game_id, $self->param( 'position' ) )
    );
  }

  method delete () {
    # not yet implemented
  }`

  method get_game (Str $game_id) {
    my $game = TTT::GameFactory->fetch(
      game_id => $game_id,
    );

    $self->return_game($game);
  }

  method new_game (Str $player) method {
    my $guid = Data::GUID->new->as_string;
    
    $self->session( game_id => $guid );
    my $game = TTT::GameFactory->create(
      game_id => $guid,
      player => $player,
    );
  }

  method make_move (Int $position) {

  }
}
