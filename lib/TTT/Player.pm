use v5.20;
use Modern::Perl;
use Moops;

class TTT::Player types TTT::Types {
  use Data::Dumper;
  use overload
    '""' => 'to_string',
    'cmp' => 'compare_string',
    '0+' => 'to_int',
    '+' => 'add',
    '-' => 'subtract',
    '<=>' => 'compare_int',
    'bool' => 'to_bool',
    '@{}' => 'to_array';
 
  has player => (
    is => 'ro',
    isa => 'Player',
  );

  sub compare_string {
    my ($left, $right) = @_;
    return $left->player cmp "$right";
  }

  method to_string {
    if (defined $self->player) {
      return $self->player;
    }

    return "";
  }

  sub add {
    my ($left, $right) = @_;

    my $int = &to_int($left);
    return $int + $right;
  }

  sub subtract {
    my ($left, $right, $invert) = @_;

    return $invert ? ($right) - (0+$left) : (0+$left) - ($right);
  }

  sub compare_int {
    my ($left, $right) = @_;
    return 0+$left <=> 0+$right;
  }

  method to_int {
    my $player = $self->player;
    if (!defined $player) {
      return 0
    } elsif ($player eq "X") {
      return 1;
    } elsif ($player eq "O") {
      return 2;
    }
  }

  method to_bool {
    if ($self->player) {
      return 1;
    }

    return 0;
  }

  method to_array {
    my @base = (0,0,0);
    $base[0+$self] = 1;

   return \@base;
  }
}
