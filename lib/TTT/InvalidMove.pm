use Modern::Perl;
use Moops;

class TTT::InvalidMove extends TTT::Error types TTT::Types {
  has 'requested_position' => (
    is => 'rw',
    isa => 'Position'
  );

  has 'current_occupant' => (
    is => 'rw',
    isa => TTT::Player
  );
}
