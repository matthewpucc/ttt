use Modern::Perl;
use Moops;

class TTT::NN {
  use Carp qw(cluck);
  use AI::NeuralNet::Simple;
  use Data::Dumper;
  $Data::Dumper::Indent = 2;
  use Clone qw(clone);
  use MooseX::Singleton;

	method nn {
		if (defined $self->{nn}) {
			return $self->{nn};
		}

		my $nn = AI::NeuralNet::Simple->new(27, 1, 3);
		$self->{nn} =  $nn;

		return $nn;
	}

  method train {
    $self->nn->train_set(
      $self->generate_permutations,
      10000,
      0.0001
    );
  }

  method generate_permutations {
    my @players = (1,2);
    my $empty = [1,0,0];
    my @sets;
    my $winner;

    for my $player (@players) {
      my $first = [0,0,0];
      $first->[$player] = 1;
      
      my $second = [0,0,0];
      $second->[2] = 1 if ($player == 1);
      $second->[1] = 1 if ($player == 2);

      my @moves = ();
      for (0..8) {
        $moves[$_] = $empty;
      }

      for my $first_move (0..8) {
        $moves[$first_move] = $first;
        push @sets, (clone(\@moves), $empty);

        for my $second_move (0..8) {
          next unless ($moves[$second_move][0]);
          $moves[$second_move] = $second;
          push @sets, (clone(\@moves), $empty);

          for my $third_move (0..8) {
            next unless ($moves[$third_move][0]);
            $moves[$third_move] = $first;
            push @sets, (clone(\@moves), $empty);

            for my $fourth_move (0..8) {
              next unless ($moves[$fourth_move][0]);
              $moves[$fourth_move] = $second;
              push @sets, (clone(\@moves), $empty);

              for my $fifth_move (0..8) {
                next unless ($moves[$fifth_move][0]);
                $moves[$fifth_move] = $first;

                #we can win starting on the fifth move
                $winner = $self->is_winning_set(\@moves);
                if ($winner) {
                  my @win_array = (0,0,0);
                  $win_array[$winner] = 1;
                  push @sets, (clone(\@moves), \@win_array);
                  $moves[$fifth_move] = $empty;
                  next;
                } else {
                  push @sets, (clone(\@moves), $empty);
                }

                for my $sixth_move (0..8) {
                  next unless ($moves[$sixth_move][0]);
                  $moves[$sixth_move] = $second;

                  $winner = $self->is_winning_set(\@moves);
                  if ($winner) {
                    my @win_array = (0,0,0);
                    $win_array[$winner] = 1;
                    push @sets, (clone(\@moves), \@win_array);
                    $moves[$sixth_move] = $empty;
                    next;
                  } else {
                    push @sets, (clone(\@moves), $empty);
                  }

                  for my $seventh_move (0..8) {
                    next unless ($moves[$seventh_move][0]);
                    $moves[$seventh_move] = $first;

                    $winner = $self->is_winning_set(\@moves);
                    if ($winner) {
                      my @win_array = (0,0,0);
                      $win_array[$winner] = 1;
                      push @sets, (clone(\@moves), \@win_array);
                      $moves[$seventh_move] = $empty;
                      next;
                    } else {
                      push @sets, (clone(\@moves), $empty);
                    }

                    for my $eigth_move (0..8) {
                      next unless ($moves[$eigth_move][0]);
                      $moves[$eigth_move] = $second;

                      $winner = $self->is_winning_set(\@moves);
                      if ($winner) {
                        my @win_array = (0,0,0);
                        $win_array[$winner] = 1;
                        push @sets, (clone(\@moves), \@win_array);
                        $moves[$eigth_move] = $empty;
                        next;
                      } else {
                        push @sets, (clone(\@moves), $empty);
                      }

                      for my $ninth_move (0..8) {
                        next unless ($moves[$ninth_move][0]);
                        $moves[$ninth_move] = $first;

                        $winner = $self->is_winning_set(\@moves);
                        my @win_array = (0,0,0);
                        $win_array[$winner] = 1;
                        # always store the ninth move since cat's are relevant to training
                        push @sets, (clone(\@moves), \@win_array);

                        $moves[$ninth_move] = $empty;
                        last;
                      }
                      $moves[$eigth_move] = $empty;
                    }
                    $moves[$seventh_move] = $empty;
                  }
                  $moves[$sixth_move] = $empty;
                }
                $moves[$fifth_move] = $empty;
              }
              push @sets, (clone(\@moves), $empty);
              $moves[$fourth_move] = $empty;
            }
            push @sets, (clone(\@moves), $empty);
            $moves[$third_move] = $empty;
          }
          push @sets, (clone(\@moves), $empty);
          $moves[$second_move] = $empty;
        }
        push @sets, (clone(\@moves), $empty);
        $moves[$first_move] = $empty;
      }
    }
    my $count2 = 0;
 
#    say Dumper([map {
#      my $return;
#      if ($count2++ %2) {
#        if ($_->[0]) {
#          $return = "0";
#        } elsif ($_->[1]) {
#          $return = "X";
#        } else {
#          $return = "O";
#        }
#      } else {
#        $return = "";
#        for my $blah (@$_) {
#          if ($$blah[0]) {
#            $return .= "0";
#          } elsif ($$blah[1]) {
#            $return .= "X";
#          } else {
#            $return .= "O";
#          }
#        }
#      }
#      $return;
#    } @sets]);

   
    say (scalar @sets)  / 2;
    my @return_data;
    my $count = 0;
    for (@sets) {
      if ($count++ % 2 == 1) {
        push @return_data, $_;
      } else {
        my @set_data;
        for (@$_) {
          push @set_data, @$_;
        }
        push @return_data, \@set_data;
      }
    }

    return \@return_data;
  }

  method is_winning_set (ArrayRef $moves) {
		if (scalar @$moves != 9) {
			cluck;
		}
    my @wins = (
      [1,1,1,0,0,0,0,0,0],
      [0,0,0,1,1,1,0,0,0],
      [0,0,0,0,0,0,1,1,1],
      [1,0,0,1,0,0,1,0,0],
      [0,1,0,0,1,0,0,1,0],
      [0,0,1,0,0,1,0,0,1],
      [0,0,1,0,1,0,1,0,0],
      [1,0,0,0,1,0,0,0,1],
    );
    
    my $winner = 0;
    WINNER_CHECK: for my $win (@wins) {
      for my $player (1..2) {
        my $match = 1;
        for my $position (0..8) {

          if ($win->[$position] && !$moves->[$position][$player]) {
            $match = 0;
            last;
          }
        }

        if ($match) {
          $winner = $player;
          last WINNER_CHECK;
        }
      }
    }

    return $winner;
  }

  method determine_winner (ArrayRef[TTT::Player] $players, ArrayRef[TTT::Position] $positions) {
		my $nn = $self->nn;
    
    my $winner = $nn->winner($self->convert_positions_to_array($positions));

    if ($winner == 0) {
      return undef;
    }
    for (@$players) {
      return $_ if ($_ == $winner);
    }
  }

  method get_best_move (TTT::Player $ai, ArrayRef[TTT::Position] $positions) {
    my $results = $self->nn->infer($self->convert_positions_to_array($positions));
    say join "",  map { $_? "$_" : "0" } @$positions;
    my $base_winner = $results->[$ai];
    my $best_next_move;
    for my $next_move (0..8) {
			my @moves = @{ clone($positions) };
      next if ($moves[$next_move]);
			$moves[$next_move]->occupy($ai);
      my $move_results = $self->nn->infer($self->convert_positions_to_array(\@moves));
      if ($move_results->[$ai] > $base_winner) {
        say $move_results->[$ai] . " " . $base_winner;
        say "new winner $ai $next_move";
        $best_next_move = $next_move;
        $base_winner = $move_results->[$ai];
      } else {
        say "$ai: " . $move_results->[$ai];
      }
    }

    say "winning with $best_next_move";

    return $positions->[$best_next_move];
  }

  method convert_positions_to_array (ArrayRef $positions) {
    my @data;

    for (@$positions) {
      push @data, @$_;
    }

    return \@data;
  }
}
