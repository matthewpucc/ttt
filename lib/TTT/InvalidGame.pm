use Modern::Perl;
use Moops;

class TTT::InvalidGame extends TTT::Error {
  has 'send_guid' => (
    is => 'rw',
    isa => Str
  );
}
