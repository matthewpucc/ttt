use Modern::Perl;
use Moops;

class TTT::Position types TTT::Types {
  use Data::Dumper;
  use overload
    'cmp' => 'compare_string',
    '""' => 'to_string',
    '+' => 'add',
    '-' => 'subtract',
    '<=>' => 'compare_int',
    '+0' => 'to_int',
    'bool' => 'to_bool',
    '@{}' => 'to_array';

  has position => (
    is => 'ro',
    isa => 'Position',
  );

  has player => (
            is =>  'rw',
    isa => InstanceOf ['TTT::Player']
  );

  method occupy (TTT::Player $player) {
    $self->player($player);
  }

  method to_bool {
    if ($self->player) {
      return 1;
    }

    return 0;
  }

  sub compare_string {
    my ($left, $right) = @_;
    return $left->player cmp "$right";
  }

  method to_string {
    return "" . $self->player;
  }

  sub add {
    my ($left, $right)  = @_;
    return ($left->position) + ($right+0);
  }

  sub subtract {
    my ($left, $right, $invert)  = @_;
    return $invert ? ($right+0) - ($left->position) : ($left->position) - ($right+0);
  }

  sub compare_int {
    my ($left, $right) = @_;
    return 0+$left <=> 0+$right;
  }

  method to_int {
    return $self->position;
  }

  method to_array {
    return $self->player ? [@{$self->player}] :[1,0,0];
  }
}
