use Modern::Perl;
use Moops;

library TTT::Types
  extends Types::Standard
  declares Position, Player {
    declare Position,
      as 'Int',
      where { $_ =~ /^[0-8]$/ };

    declare Player,
      as 'Str',
      where { !defined $_ || $_ =~ /^[XO]$/ };
  }

1;
