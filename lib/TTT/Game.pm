use Modern::Perl;
use Moops;

class TTT::Game types TTT::Types {
  use TTT;
  use TTT::Player;
  use TTT::Position;
  use Data::Dumper;
  use Carp;

  has app => (
    is => 'ro',
    isa => InstanceOf ['TTT'],
    lazy => 1,
    builder => sub {
      return TTT->new();
    }
  );

	has state => (
		is => 'rw',
		isa => 'Str',
		init_arg => 'initial_state'
  );

  has guid => (
    is => 'ro',
    isa => Str
  );

  has winner => (
    is => "rw",
    isa => InstanceOf ['TTT::Player'],
  );

  has user => (
    is => 'ro',
		isa => InstanceOf ['TTT::Player'],
  );

  has ai => (
    is => 'ro',
    isa => InstanceOf ['TTT::Player'],
    lazy => 1,
    builder => sub {
      my $ai;

      if (shift->user eq "X") {
        $ai = "O";
      } else {
        $ai = "X";
      }

      return TTT::Player->new(player => $ai);
    }
  );

  has players => (
    is => 'ro',
	  isa => 'ArrayRef',
    lazy => 1,
    builder => sub {
      my $self = shift;
		 	my @players;
			push @players, $self->ai;
			push @players, $self->user;
      return \@players;
    }
  );
  
  has x => (
    is => 'ro',
    lazy => 1,
    builder => sub {
      my $self = shift;
      my $players = $self->players;
      for (@$players) {
        if ($_ eq "X") {
          return $_;
        }
      }
    }
  );
  
  has o => (
    is => 'ro',
    lazy => 1,
    builder => sub {
      my $self = shift;
      my $players = $self->players;
      
      for (@$players) {
        if ($_ eq "O") {
          return $_;
        }
      }
    }
  );

  method BUILDARGS (ClassName $class: %args) {
		if (defined $args{user} && $args{user} =~ /^[XO]$/) {
			$args{user} = TTT::Player->new(player => $args{user});
		} else {
			delete $args{user}
    }

		return \%args;
  }

  method BUILD  {
    $self->{positions} = [];

    my @state;
    if (defined $self->state) {
      @state = split //, $self->state;
    }

    for (0..8) {
      my $player = $state[$_];
      my $opts = {
        position => $_
      };
      if ($player eq "X") {
        $opts->{player} = $self->x;
      } elsif ($player eq "O") {
        $opts->{player} = $self->o;
      }
      $self->{positions}->[$_] = TTT::Position->new(%$opts);
    }
  }

  method get_position (Position $position) {
    return $self->{positions}->[$position];
  }
  
  method ai_move {
    my $best_move = $self->app->nn->get_best_move($self->ai, $self->{positions});
    $self->move($best_move, $self->ai);
  }

  method user_move (Position $position) {
    $self->move($position, $self->user);
  } 

  multi method move (Position $position, TTT::Player $player) {
    my $board_position = $self->get_position($position);
 	  return $self->make_move($board_position, $player);
  }

  multi method move (TTT::Position $position, TTT::Player $player) {
   return $self->make_move($position, $player);
	}

	method make_move (TTT::Position $board_position, TTT::Player $player) {
    if ($board_position) {
      TTT::InvalidMove->throw(
        requested_position => $board_position,
        current_occupant => $board_position->player
      );   
    }

    $board_position->occupy($player);
    my $winner = $self->check_winner;
    my @states = split //, $self->state;
    $states[$board_position] = "$player";
    $self->state(join("", @states));
    if ($winner) {
      $self->winner($winner);
    }
  }

  method check_winner {
    my $winner = $self->app->nn->determine_winner($self->players, $self->{positions});

    for (@{$self->players}) {
      if ($_ == $winner) {
        return $_;
      }
    }


    if ($self->get_remaining_moves == 0) {
      return 0;
    }

    return undef;
  }

  method get_remaining_moves () {
    return () = grep { !$_ } @{$self->{postions}};
  }
}
