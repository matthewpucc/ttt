use Modern::Perl;
use Moops;

class ::TTT {
  use Data::GUID;
  use IPC::Shareable;
  use Try::Tiny;

	use TTT::NN;

  has 'ipc_options' => (
    is => 'ro',
    lazy => 1,
    default => sub {
      return {
        create    => 1,
        exclusive => 0,
        mode      => 0644,
        destroy   => 1,
      };
    }
  );

  has data => (
    is => 'ro',
    lazy => 1,
    builder => sub {
      my $options = shift->ipc_options;

      my %data;
      tie %data, 'IPC::Shareable', 'ttt_data', $options;

      return \%data;
    }
  );

  has nn => (
    is => 'ro',
    lazy => 1,
    builder => sub {
      my $self = shift;

      my $nn = TTT::NN->instance;
      try {
				$nn->train();
			} catch {
				print STDERR $_;
 				die $_;
			};

      return $nn;
    }
  );

  has games => (
    is => 'ro',
    lazy => 1,
    builder => sub {
      my $self = shift;
      my %games;

      my $options = $self->ipc_options;
      tie %games, 'IPC::Shareable', 'ttt_game_data', $options;

      return \%games;
    }
  );

  multi method add_game (TTT::Game $game) {
    $self->games->{$game->guid} = $game;
  }

  multi method add_game (Str $game_state, Str $user) {
    my $guid = Data::GUID->new->as_string;
    my $game = TTT::Game->new(initial_state => $game_state, guid => $guid, user => $user);
    $self->games->{$guid} = $game;
  }

  method get_game (Str $guid) {
    my $game = $self->games->{$guid};
    unless (defined $game) {
      TTT::InvalidGame->throw(
        send_guid => $guid
      );
    }

    return $game;
  }
}