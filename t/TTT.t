#!/usr/bin/env perl
use Modern::Perl;

use Test::More;
use Scalar::Util 'blessed';
use Data::Dumper;
$Data::Dumper::Indent = 2;
use Test::Fatal;
use Data::GUID;
use Try::Tiny;
use TTT;


my $added_games = [];
my $ttt = &sut->new();

&test_initialization;
&test_add_game;
&test_ipc_cache;
&test_get_game;

done_testing;


sub sut { 'TTT' }

sub test_initialization {
	ok(blessed($ttt) && $ttt->isa('TTT'), "Is a blessed TTT Object") or diag "Bad TTT Object " . Dumper($ttt);
}

sub test_add_game {
	my $game;
	is(exception {$game = $ttt->add_game("000000000", "X");}, undef, "Can successfully add a game");
	push @{$added_games}, $game;

	ok(defined exception {$ttt->add_game({});}, "Can not game game with invalid params");


	$game = undef;
	try {
		use TTT::Game;
		$game = TTT::Game->new(guid => Data::GUID->new->as_string, initial_state => "000000000");
	} catch {};

	SKIP: {
		skip "Could not create game to test api", 1 unless defined $game;

		is(exception {$ttt->add_game($game)}, undef, "Can add TTT::Game instance");
	};
}

sub test_ipc_cache {
  $ttt->games->{test} = 123;
	
	my $new_ttt = TTT->new;
	
	is($ttt->games->{test}, $new_ttt->games->{test}, "Cache works across multiple instances of TTT");
	
	my $guid = $added_games->[0]->guid;
	is($ttt->games->{$guid}->guid, $new_ttt->games->{$guid}->guid, "Cache works for blessed refs");
}

sub test_get_game {
	my $guid = $added_games->[0]->guid;
	
	is($ttt->get_game($guid)->guid, $guid, "Get game returns same game");
}

1;