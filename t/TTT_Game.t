#!/usr/bin/env perl

use Modern::Perl;
use Test::More;
use Data::GUID;
use Data::Dumper;

use File::FindLib('../lib');

use TTT::Game;

&test_app;
&test_ai;
&test_get_position;
&test_ai_move;

END {
  done_testing;
}

sub test_app {
  my  $game = &create_empty_game;


  ok($game->isa('TTT::Game'), "Game is an instance of TTT::Game");
  ok($game->app->isa('TTT'), "App is an instance of TTT");
}

sub test_ai {
  my $game = &create_empty_game;

  is($game->ai, 'O', "AI is O when user is X") or "AI should be O but was $game->ai";

  $game = &create_empty_game("O");

  ok($game->ai eq "X", "AI is X when user is O") or diag "AI should be X but was " . $game->ai;
}

sub test_get_position {
  my $game = &create_empty_game("X", "XOXOXOXOX");
  for (0..8) {
    is("".$game->get_position($_), $_%2 ? "O" : "X", "Position $_ player is  correct");
    is(0+$game->get_position($_), $_, "Position $_ position is correct");
  }
}

sub test_ai_move {
  my $game = &create_empty_game("O", "0XXOO0000");

  ok(ref($game) && $game->isa("TTT::Game"), "Game is set up");
 
  $game->ai_move;
  is ($game->winner, $game->ai, "AI Wins");

  $game = &create_empty_game("O", "00OXX000O");
  $game->ai_move;
  is($game->winner, $game->ai, "AI Won again");
}

sub create_empty_game {
  my $user = shift // "X";
  my $initial_state = shift // "000000000";
  return TTT::Game->new(guid => Data::GUID->new->as_string, initial_state => $initial_state, user =>  $user);
}
