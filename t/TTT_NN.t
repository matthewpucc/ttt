#!/usr/bin/env perl

use Modern::Perl;
use Test::More;
use Data::GUID;
use Data::Dumper;

use File::FindLib('../lib');

use TTT::NN;

&test_instanciation;
&test_ai;

END {
  done_testing;
}

sub test_instanciation {
  my  $game = &create_empty_nn;


  ok($game->isa('TTT::NN'), "Game is an instance of TTT::Game");
}

sub test_ai {
  my $nn = &create_empty_nn;
  is(ref($nn->generate_permutations), "ARRAY", "Generate permutations returns array ref");
  
}

sub test_get_position {
  my $game = &create_empty_game("X", "XOXOXOXOX");
  for (0..8) {
    is("".$game->get_position($_), $_%2 ? "O" : "X", "Position $_ player is  correct");
    is(0+$game->get_position($_), $_, "Position $_ position is correct");
  }
}

sub test_ai_move {
  my $game = &create_empty_game("O", "0XXOO0000");

  ok(ref($game) && $game->isa("TTT::Game"), "Game is set up");
 
  $game->ai_move;
  is ($game->winner, $game->ai, "AI Wins");
}

my $nn;
sub create_empty_nn {
  return $nn if $nn;
  $nn = TTT::NN->instance();
  return $nn;
}
